/*
 * File:   main.c
 * Author: kolpe
 *
 * Created on March 25, 2019, 8:47 PM
 */

#pragma config JTAGEN=OFF, GCP=OFF, GWRP=OFF, FWDTEN=OFF, ICS=PGx2, \
               IESO=OFF, FCKSM=CSDCMD, OSCIOFNC=OFF, POSCMOD=HS, \
               FNOSC=PRIPLL, PLLDIV=DIV3, IOL1WAY=ON
#include <string.h>

#include <stdio.h>
#include "xc.h"
#include "led.h"
#include "usbserial.h"
#include "delay.h"
#include "display.h"
unsigned bit_size_of_type=sizeof(char)*8;

void init_UART (void){
    
    U1MODEbits.UARTEN=1; 
    U1MODEbits.USIDL=0;
    U1MODEbits.WAKE=1;
    U1MODEbits.UEN0=1;
    U1MODEbits.UEN1=0;
    U1MODEbits.IREN=0;
    U1MODEbits.ABAUD=0;
    U1MODEbits.LPBACK=0;
    U1MODEbits.RTSMD=0;
    U1MODEbits.STSEL=0;
    U1MODEbits.PDSEL=0;
    U1MODEbits.BRGH=1;
      
    U1BRG=417;
    
    U1STAbits.UTXISEL0 = 0;            // Interrupt after one TX character is transmitted
    U1STAbits.UTXISEL1 = 0;
   
    U1MODEbits.UARTEN = 1;              // Enable UART
    U1STAbits.UTXEN = 1;
    RPOR12bits.RP24R=3;
    RPINR18bits.U1RXR = 22;
    delay_loop(105);  
}
void init_components (void){
    usbserial_init();
    disp_init(); 
    set_color(1);  
    init_UART();
}
unsigned char serial_read (){
    while( U1STAbits.URXDA == 0 ){
          }
  return (unsigned char) U1RXREG;
}


unsigned char serial_read_non_block (){
    if( U1STAbits.URXDA == 0 ){
        return 0;
          }
  return (unsigned char) U1RXREG;
}

void serial_write_int_hex ( int print, int precision){
    char to_print[precision];
    int i=0; 
    
    snprintf(to_print, precision, "%02X",print);
    while(to_print[i]!='\0' ){ 
       U1TXREG = to_print[i];
       i++;
       while(U1STAbits.UTXBF == 1){
       }
    }
    
}
void print_int_hex ( int print, int precision ){
    char to_print[precision];
    snprintf(to_print, precision, "%02X",print);
    disp_str(to_print);
}





typedef struct Bit_position_t{
    int byte;
    int bit_pos;
    unsigned char bit;
} Bit_position;


unsigned int len_of_number(unsigned char a[],unsigned int lenght_a){
  unsigned int lenght=0;
  int i;
  for ( i = 0; i < lenght_a; ++i)
  {
    if(a[i]){
      lenght=i+1;

    }
  }
  return lenght;
}


Bit_position most_significant_bit_pos (unsigned char a [],unsigned length_a ){
  int i=0,k=7;
  unsigned char j=0x80;
  Bit_position pos;


  for (i=length_a-1; i >= 0; --i)
  { 
    while(j){
      if(a[i]&j){
        pos.byte=i;
        pos.bit=j;
        pos.bit_pos=k;
        return  pos;
      }
       j>>=1;
       k--;
    }
     j=0x80;
     k=7;
   
  }
  pos.bit=0;
  pos.byte=0;
  pos.bit_pos=0;
  return pos;
}

void add(unsigned char a[], unsigned int lenght_a,unsigned char b[], unsigned  int lenght_b,unsigned char p[],int lenght_p,unsigned char c[], unsigned int lenght_c );
//a>=b
int geq (unsigned char a[],unsigned int lenght_a, unsigned char b[], unsigned int lenght_b){
unsigned int real_lenght_a=len_of_number(a,lenght_a);
unsigned int real_lenght_b=len_of_number(b,lenght_b);
  int i;

    


  if(real_lenght_a>real_lenght_b){
    return 1;
  }
  if (real_lenght_a<real_lenght_b)
  {
   return 0;
  }

  unsigned int lenght_res;
  if(lenght_a>lenght_b){
    lenght_res=lenght_b;
  }else{
    lenght_res=lenght_a;
  }

  for ( i = lenght_res-1; i >= 0; --i)
  {
    if( a[i]>b[i]){
      return 1;
    }
    if(a[i]<b[i]){
      return 0;
    }
  }
  return 1;

}







int neq (unsigned char a[],unsigned int lenght_a, unsigned char b[], unsigned int lenght_b){
unsigned int real_lenght_a=len_of_number(a,lenght_a);
unsigned int real_lenght_b=len_of_number(b,lenght_b);
  int i;

    


  if(real_lenght_a!=real_lenght_b){
    return 1;
  }
 

  for ( i = real_lenght_a-1; i >= 0; --i)
  {
    if( a[i]!=b[i]){
      return 1;
    }
  }
  return 0;

}





int geq_signed (unsigned char a[],unsigned int lenght_a, unsigned char b[], unsigned int lenght_b){
if(a[lenght_a-1]&0x80){
  if(b[lenght_b-1]&0x80){
    return !geq(a,lenght_a,b, lenght_b);
  }
  else{
    return  0;
  }
}
else{
  if(b[lenght_b-1]&0x80){
    return 1;
  }
  else{
    return  geq(a,lenght_a,b, lenght_b);
  }

}

}

//b=b-a


int zero(unsigned char a[],unsigned int lenght_a){
  int i;
    for( i=0;i<lenght_a;i++){
    if(a[i]){
      return 0;
    }
  }
  return 1;
}

void sub(unsigned char a[],unsigned int lenght_a,unsigned char b[], unsigned int lenght_b,unsigned char * p, unsigned int lenght_p,unsigned char c[], unsigned int lenght_c ){
  int carry=1;
  int i;
  int j;


 
unsigned int lenght_res;
  if(lenght_a>lenght_b){
    lenght_res=lenght_a;
  }else{
    lenght_res=lenght_b;
  }
  lenght_res++;
  unsigned char res [lenght_res];

   unsigned char pl1=0,pl2=0;
  for(i=0;i<lenght_res;i++){
    if(i<lenght_a){
      pl1=~a[i];
    }
    else{
      pl1=0xff;
    }

    if(i<lenght_b){
      pl2=b[i];
    }
    else{
      pl2=0;
    }

   res[i]=pl1+pl2+carry;
   if((pl1+pl2+carry)&0x100){
      carry=1;
   }
   else{
    carry=0;
   }
  }




if(res[lenght_res-1]&0x80){

    

  add(p,lenght_p,res,lenght_res-1,p,lenght_p,res,lenght_res-1);
}
   for ( j= 0; j < lenght_c; ++j)
    {
       c[j]=res[j];
    }

  

 return; 
}


int sub_no_mod(unsigned char a[],unsigned int lenght_a,unsigned char  b[], unsigned int lenght_b,unsigned char * c, unsigned int lenght_c ){
  int carry=1;
  int i;
  int j;

   /*    for ( int j = 9; j >=0; --j)
    {
       cout<<"0x"<<hex<<(int)b[j]<<",";
    }
     cout<<"firs"<<endl;*/
 
unsigned int lenght_res;
  if(lenght_a>lenght_b){
    lenght_res=lenght_a;
  }else{
    lenght_res=lenght_b;
  }
  lenght_res++;
  unsigned char res [lenght_res];

   unsigned char pl1=0,pl2=0;
  for(i=0;i<lenght_res;i++){
    if(i<lenght_a){
      pl1=~a[i];
    }
    else{
      pl1=0xff;
    }

    if(i<lenght_b){
      
      pl2=b[i];
    }
    else{
      pl2=0;
    }

   res[i]=pl1+pl2+carry;

   if((pl1+pl2+carry)&0x100){

      
      carry=1;
   }
   else{
    carry=0;
   }
  }





   for ( j= 0; j < lenght_c; ++j)
    {
       c[j]=res[j];
    }


  

  if(res[lenght_res-1]&0x80){

  return 1;
}

 return 0; 
}

//b=b+a 
void add(unsigned char a[], unsigned int lenght_a,unsigned char b[], unsigned  int lenght_b,unsigned char  p[],int lenght_p,unsigned char c[], unsigned int lenght_c ){

 unsigned int lenght_res;
  if(lenght_a>lenght_b){
    lenght_res=lenght_a;
  }else{
    lenght_res=lenght_b;
  }

 int i;
 int j;

  unsigned char res [lenght_res+1];
  int carry=0;
  
  int pl1=0,pl2=0;
  for(i=0;i<lenght_res;i++){
    if(i<lenght_a){
      pl1=a[i];
    }
    else{
      pl1=0;
    }

    if(i<lenght_b){
      pl2=b[i];
    }
    else{
      pl2=0;
    }

   res[i]=pl1+pl2+carry;
   if((pl1+pl2+carry)&0x100){
      carry=1;
   }
   else{
    carry=0;
   }
  }
if( carry==1){
  res[i]=1;
}

     


if(geq(res,lenght_res,p, lenght_p)){

  
  sub(p,lenght_p,res,lenght_res,p,lenght_p,res,lenght_res);
}
   for ( j= 0; j < lenght_c; ++j)
    {
       c[j]=res[j];
    }


 
}


void add_no_mod(unsigned char a[], unsigned int length_a,unsigned char b[], unsigned  int length_b,unsigned char c[], unsigned int length_c ){

 unsigned int length_res;
  if(length_a>length_b){
    length_res=length_a;
  }else{
    length_res=length_b;
  }

 int i;
  int j;

 unsigned char res [length_res+1];
 int carry=0;
  
 int pl1=0,pl2=0;
  for(i=0;i<length_res;i++){
    if(i<length_a){
      pl1=a[i];
    }
    else{
      pl1=0;
    }

    if(i<length_b){
      pl2=b[i];
    }
    else{
      pl2=0;
    }

   res[i]=pl1+pl2+carry;

   if((pl1+pl2+carry)&0x100){
      carry=1;
   }
   else{
    carry=0;
   }
  }

 if( carry==1){
   res[i]=1;
 }

     

for ( j= 0; j < length_c; ++j)
{
   c[j]=res[j];
}


 
}

void div_by_two(unsigned char a [],unsigned int lenght_a){
  int carry=0;
  int tmp=0;
  int i;
  for( i=lenght_a-1;i>=0;i--){

   tmp=a[i]&1;
     

    a[i]>>=1;
    if(carry){
      a[i]+=0x80;
    }
    carry=tmp;
   
  }
}
void mul_by_two(unsigned char a [],unsigned int lenght_a){
  int carry=0;
  int tmp=0;
  int i;

  for( i=0;i<lenght_a;i++){

   tmp=a[i]&0x80;
     

    a[i]<<=1;
    if(carry){
      a[i]+=0x1;
    }
    carry=tmp;
   
  }
}


void mul_by_two_mod(unsigned char a [],unsigned int lenght_a,unsigned char  p[],int lenght_p){
  int carry=0;
  int tmp=0;
  int j;
  int i;
  int lenght_res=lenght_a+1;



  unsigned char res [lenght_res];
  for( i=0;i<lenght_a;i++){

   tmp=a[i]&0x80;

     

   res[i]=a[i]<<1;
    if(carry){
      res[i]+=0x1;
    }
    carry=tmp;
   
  }
   if(carry){
      res[lenght_res-1]=0x1;
  }else{
    res[lenght_res-1]=0x0;
  }


 


  if(geq(res,lenght_res,p, lenght_p)){


    sub_no_mod(p,lenght_p,res,lenght_res,res,lenght_res);
  }
   for ( j= 0; j < lenght_a; ++j)
    {
       a[j]=res[j];
    }

}



void mongomery_mult(unsigned char a [],unsigned int length_a,unsigned char b[],unsigned length_b, unsigned char p[],unsigned int length_p,unsigned char res [],unsigned int length_res){
 
int length_x= length_res+1;
  unsigned char x [length_x];
  int j;
   for ( j = 0; j < length_x; ++j)
    {
      x[j]=0;
    }

  Bit_position pos=most_significant_bit_pos(p,length_p);

  int i = 0;
  int l;
  unsigned char bit = 1;
  for( l=0;l < 8*pos.byte+pos.bit_pos+1;l++){
  //  cout<<dec<<l<<endl;

    if(a[i]&bit){

      add_no_mod(b,length_b,x,length_x,x,length_x);

    }

    if(x[0]&1){

      add_no_mod(p,length_p,x,length_x,x,length_x);

    }
    div_by_two(x,length_x);

    bit<<=1;
  
    if(!bit){
       i++;
       bit=1;
     }
  

  }

    if (geq(x,length_x, p,length_p)) {

      sub(p,length_p,x,length_x,p,length_p,x,length_x);
    }

    for (i = 0; i < length_res; ++i)
    {
      res[i]=x[i];
    }

  }







void computeR2 ( unsigned char p[],unsigned lenght_p,unsigned char x [],unsigned lenght_x){
 int i=0;
 Bit_position pos=most_significant_bit_pos(p,lenght_p);

//cout<<dec<<pos.byte<< "        " <<pos.bit_pos<<"    "<<hex<<(int)pos.bit<<endl;


 int lenght_r=pos.byte+1;
 unsigned char r[lenght_r];
 for(i=0;i<lenght_r;i++){
  r[i]=0;
 }
 r[pos.byte]=pos.bit;

 for (i=0;i<8*pos.byte+pos.bit_pos+2;i++){
  add(r,lenght_r,r,lenght_r,p,lenght_p,r,lenght_r);


 }

 for (i = 0; i < lenght_r; ++i)
 {
  x[i]=r[i];
 }
}


void to_mongomery(unsigned char a [],unsigned lenght_a,unsigned char r2 [],unsigned lenght_r2, unsigned char p[],unsigned lenght_p,unsigned char x [],unsigned lenght_x){
  mongomery_mult(a,lenght_a,r2,lenght_r2,p,lenght_p,x,lenght_x);
}
void from_mongomery(unsigned char a [],unsigned lenght_a, unsigned char p[],unsigned lenght_p,unsigned char x [],unsigned lenght_x){
  unsigned char one[lenght_p];
  int i;
  for ( i = 0; i < lenght_p; ++i)
  {
    one[i]=0;
  }
one[0]=1;
  mongomery_mult(one,1,a,lenght_a,p,lenght_p,x,lenght_x);
 
}



void Montgomery_Modular_Inverse(unsigned char a [],unsigned int length_a,unsigned char b[],unsigned length_b, unsigned char p[],unsigned int length_p,unsigned char x [],unsigned int length_x){

 Bit_position pos=most_significant_bit_pos(p,length_p);

int length_u=length_a+5;
unsigned char u[length_u];
int i;

for (i = 0; i < length_u; ++i)
{
  u[i]=0;
}

for (i = 0; i < length_a; ++i)
{
  u[i]=a[i];
}

int length_v=length_b+5;
unsigned char v[length_v];


for (i = 0; i < length_v; ++i)
{
  v[i]=0;
}

for (i = 0; i < length_b; ++i)
{
  v[i]=b[i];
}

int length_r=length_a+5;
unsigned char r[length_r];


for ( i = 0; i < length_r; ++i)
{
  r[i]=0;
}


int length_s=length_a+5;
unsigned char s[length_s];

for ( i = 0; i < length_s; ++i)
{
  s[i]=0;
}
s[0]=1;
int positive=0;
int k=0;

 while (!positive && !zero(v,length_v) ){

 
   if (!(u[0]&1)) {
     div_by_two(u,length_u);
     mul_by_two(s,length_s);
   }
   else{
     if (!(v[0]&1)) {
       div_by_two(v,length_v);
       mul_by_two(r,length_r);
     }else{
        if(!geq(v,length_v,u,length_u) ){
  
           sub_no_mod(v,length_v,u,length_u,u,length_u);
           div_by_two(u,length_u);
           add_no_mod(r,length_r,s,length_s,r,length_r);
           mul_by_two(s,length_s);       

        }
        else{
           positive=sub_no_mod(u,length_u,v,length_v,v,length_v);
           div_by_two(v,length_v);
           add_no_mod(r,length_r,s,length_s,s,length_s);
           mul_by_two(r,length_r);

        }
      }


   }
  k++;
 }


 if(geq(r,length_r,a, length_a)){
    sub_no_mod(a,length_a,r,length_r,r,length_r);
  }




 sub_no_mod(r,length_r,a,length_a,r,length_r);


for( i=0;i<2*(8*pos.byte+pos.bit_pos+1)-k;i++){

   mul_by_two_mod (r,length_r,a,length_a);
   
}

 for (i = 0; i < length_x; i++)
 {
  x[i]=r[i];
 }
  
}





void add_point( unsigned char a_M [],unsigned int length_a_M,unsigned char b_M[],unsigned length_b_M,unsigned char p[],unsigned length_p,
     unsigned char x_P_MT[],unsigned int length_x_P_M,unsigned char y_P_MT[],unsigned length_y_P_M,unsigned char x_Q_MT[],unsigned length_x_Q_M, unsigned char y_Q_MT[],unsigned length_y_Q_M,
     unsigned char res_x[],unsigned length_res_x,  unsigned char res_y[],unsigned length_res_y){

int i;
unsigned char  x_P_M[length_x_P_M];
unsigned char  y_P_M[length_y_P_M];
unsigned char  x_Q_M[length_x_Q_M];
unsigned char  y_Q_M[length_y_Q_M];

      memcpy(x_P_M,x_P_MT,length_x_P_M);
      memcpy(y_P_M,y_P_MT,length_x_P_M);
       memcpy(x_Q_M,x_Q_MT,length_x_Q_M);
      memcpy(y_Q_M,y_Q_MT,length_x_Q_M);
 
 
    unsigned length_lam=length_p;
    unsigned char lam[length_lam];

    unsigned length_tmp=length_p;
    unsigned char tmp[length_tmp];


    for ( i = 0; i < length_tmp; ++i)
    {
     tmp[i]=0;
    }
 
    unsigned length_tmp2=length_p;
    unsigned char tmp2[length_tmp2];

    for (i = 0; i < length_tmp2; ++i)
    {
      tmp2[i]=0;
    }
 

    


    if( zero(x_P_M,length_x_P_M) &&   zero(y_P_M,length_y_P_M)){
      memcpy(res_x,x_Q_M,length_res_x);
      memcpy(res_y,y_Q_M,length_res_y);
      return;
    }

    if( zero(x_Q_M,length_x_Q_M) &&   zero(y_Q_M,length_y_Q_M)){
      memcpy(res_x,x_P_M,length_res_x);
      memcpy(res_y,y_P_M,length_res_y);
      return;
    }


 


  if( neq(x_P_M,length_x_P_M,x_Q_M,length_x_Q_M) ){

     sub(y_Q_M,length_y_Q_M,y_P_M,length_y_P_M,p,length_p,tmp,length_tmp);
 
     sub(x_Q_M,length_x_Q_M,x_P_M,length_x_P_M,p,length_p,tmp2,length_tmp2);
 
     Montgomery_Modular_Inverse(p,length_p,tmp2,length_tmp2,p,length_p,tmp2,length_tmp2);

     mongomery_mult(tmp,length_tmp,tmp2,length_tmp2,p,length_p,lam,length_lam);
       
 
  }

  
  else{
    if( neq(y_P_M,length_y_P_M,y_Q_M,length_y_Q_M)){

      for ( i = 0; i < length_res_y; ++i)
      {
       res_y[i]=0;
      }

       for (i = 0; i < length_res_x; ++i)
      {
       res_x[i]=0;
      }
      return;
    } 
    if( zero(y_Q_M,length_y_Q_M)) {

      for (i = 0; i < length_res_y; ++i)
      {
       res_y[i]=0;
      }

       for (i = 0; i < length_res_x; ++i)
      {
       res_x[i]=0;
      }
      return;

    } 

  


    mongomery_mult(x_Q_M,length_x_Q_M,x_Q_M,length_x_Q_M,p,length_p,tmp,length_tmp);  

    memcpy(tmp2,tmp,length_tmp2);
    mul_by_two_mod(tmp,length_tmp,p,length_p);

    add(tmp,length_tmp,tmp2,length_tmp2,p,length_p,tmp,length_tmp);

    add(tmp,length_tmp,a_M,length_a_M,p,length_p,tmp,length_tmp);

  

    memcpy(tmp2,y_P_M,length_tmp2);
    mul_by_two_mod(tmp2,length_tmp2,p,length_p);

    Montgomery_Modular_Inverse(p,length_p,tmp2,length_tmp2,p,length_p,tmp2,length_tmp2);

    mongomery_mult(tmp,length_tmp,tmp2,length_tmp2,p,length_p,lam,length_lam);


  
      
    
  }
 





 

  mongomery_mult(lam,length_lam,lam,length_lam,p,length_p,res_x,length_res_x);


  sub(x_P_M,length_x_P_M, res_x,length_res_x,p,length_p,res_x,length_res_x);

  sub(x_Q_M,length_x_Q_M, res_x,length_res_x,p,length_p,res_x,length_res_x);

  

   sub( res_x,length_res_x,x_Q_M,length_x_Q_M,p,length_p,res_y,length_res_y);





   mongomery_mult(lam,length_lam,res_y,length_res_y,p,length_p,res_y,length_res_y);

   

   sub( y_Q_M,length_y_Q_M,res_y,length_res_y,p,length_p,res_y,length_res_y);



  
}
void SpeedupScalarMultiply(unsigned char a [],unsigned int length_a,unsigned char b[],unsigned length_b,unsigned char p[],unsigned length_p,
     unsigned char x_P [],unsigned int length_x_P, unsigned char y_P[],unsigned length_y_P, unsigned char n[],unsigned length_n,
     unsigned char res_x[],unsigned length_res_x,  unsigned char res_y[],unsigned length_res_y ){
int i;
unsigned length_t3=length_n+1;
unsigned char t3[length_t3];


memcpy(t3,n,length_n);
t3[length_n]=0;


unsigned length_x_S=length_x_P;
unsigned char x_S[length_x_S];

unsigned length_y_S=length_y_P;
unsigned char y_S[length_y_S];

if(n==0){
      for ( i = 0; i < length_res_y; ++i)
      {
       res_y[i]=0;
      }

       for (i = 0; i < length_res_x; ++i)
      {
       res_x[i]=0;
      }
      return;
}



unsigned length_x_P_min=length_x_P;
unsigned char x_P_min[length_x_P_min];

unsigned length_y_P_min=length_y_P;
unsigned char y_P_min[length_y_P_min];

memcpy(x_P_min,x_P,length_x_P);

sub_no_mod(y_P,length_y_P,p,length_p,y_P_min,length_y_P_min);



      for ( i = 0; i < length_res_y; ++i)
      {
       x_S[i]=x_P[i];
      }

      for (i = 0; i < length_res_x; ++i)
      {
       y_S[i]=y_P[i];
      }
     

mul_by_two(t3,length_t3);

add_no_mod(t3,length_t3,n,length_n,t3,length_t3);



  Bit_position pos=most_significant_bit_pos(t3,length_t3);

int l;

  unsigned char bit = pos.bit;
  i=pos.byte;
  bit>>=1;
  if(!bit){
       i--;
       bit=0x80;
  }
  for(  l= 8*pos.byte+pos.bit_pos-1;l >0 ;l--){

   
    
     add_point(  a,length_a, b,length_b, p,length_p,
                 x_S,length_x_S, y_S,length_y_S, x_S,length_x_S, y_S,length_y_S,
                 x_S,length_x_S, y_S,length_y_S);


    if(i<length_n){
        if(t3[i]&bit && !(n[i]&bit)){
           
         add_point(  a,length_a, b,length_b, p,length_p,
                 x_S,length_x_S, y_S,length_y_S, x_P,length_x_P, y_P,length_y_P,
                 x_S,length_x_S, y_S,length_y_S);
    
    
        }
        if(!(t3[i]&bit) && n[i]&bit){



             add_point(  a,length_a, b,length_b, p,length_p,
                 x_S,length_x_S, y_S,length_y_S, x_P_min,length_x_P_min, y_P_min,length_y_P_min,
                 x_S,length_x_S, y_S,length_y_S);
    
        }
      }else{
         if(t3[i]&bit){

           add_point(  a,length_a, b,length_b, p,length_p,
                 x_S,length_x_S, y_S,length_y_S, x_P,length_x_P, y_P,length_y_P,
                 x_S,length_x_S, y_S,length_y_S);
    

        }
      }


    bit>>=1;
    if(!bit){
       i--;
       bit=0x80;
     }
  

  }


 for ( i = 0; i < length_x_S; ++i)
      {
       res_y[i]=y_S[i];
      }

       for (i = 0; i < length_res_x; ++i)
      {
       res_x[i]=x_S[i];
      }
      return;



}



void shift(unsigned char shifter [],unsigned int length_shifter, unsigned char mask [],unsigned int length_mask){
int i;
int feed_back;
feed_back=shifter[0]&1;
div_by_two(shifter,length_shifter);
if(feed_back){
  for (i= 0; i < length_shifter; ++i)
  {
    shifter[i]^=mask[i];
  }
}


  return;
}






int main(void) {
    
/* 0,0 Bod v nekonecu */
unsigned char p[10] = { 0xf5,0x9c,0x89,0xca,0x2a,0x41,0x77,0x51,0xce,0x62 }; //1
unsigned char a[10] = { 0x3c,0x73,0x45,0xbc,0xb1,0xdd,0x6d,0x5e,0xc9,0x39 }; //a
unsigned char b[10] = { 0xd1,0xe,0x1c,0x5a,0x9d,0xe8,0x80,0xd8,0x16,0x1f };  //b 

unsigned char x_P[10] = { 0x7d,0x5,0x75,0x84,0x20,0x1c,0x20,0x4b,0x5d,0x31 }; //2
unsigned char y_P[10] = { 0xa,0x45,0x52,0x2,0x37,0xab,0xf5,0x3d,0x5f,0x3 };  //3

//6 add 
unsigned char res_x [10];
unsigned char res_y [10];
int len=10;
unsigned char ALICE_x [10];
unsigned char ALICE_y [10];

unsigned char BOB_x [10];
unsigned char BOB_y [10];

unsigned char ALICE_ka [10];
unsigned char R2[len];

unsigned length_a_M=len;
    unsigned char a_M[length_a_M];

    unsigned length_y_P_M=len;
    unsigned char y_P_M[length_y_P_M];

    unsigned length_x_P_M=len;
    unsigned char x_P_M[length_x_P_M];


   
  computeR2(p,len,R2,len);
  to_mongomery (a,len,R2,len,p,len,a_M,len);
  to_mongomery (x_P,len,R2,len,p,len,x_P_M,len);
  to_mongomery (y_P,len,R2,len,p,len,y_P_M,len);
 
  
  

unsigned char mask[10]={0x00,0x00,0x01,0x04,0x00,0x00,0x00,0x00,0x00,0x84};
unsigned char shifter[10]={0x01,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};


int i;





delay_loop(10000);
    init_components();
    delay_loop(10000);
  
    serial_read();
    







while(1){
    unsigned char function_num=serial_read_non_block(); //Read function number
      if(function_num==0x01){  
        int i;
         for ( i=len-1;i>=0;i--){
            p[i]=serial_read();
        }
               
        
        for ( i=len-1;i>=0;i--){
            print_int_hex((int)p[i],6);
            serial_write_int_hex((int)p[i],6);
        }
        disp_char('\n');
        
      }
          if(function_num==0x0a){  
        int i;
        for ( i=len-1;i>=0;i--){
            a[i]=serial_read();
          
        }
               
         to_mongomery (a,len,R2,len,p,len,a_M,len);
        for ( i=len-1;i>=0;i--){
            print_int_hex((int)a[i],6);
            serial_write_int_hex((int)a[i],6);
        }
        disp_char('\n');
       
      }
    
        if(function_num==0x0b){   
        int i;
        for ( i=len-1;i>=0;i--){
            b[i]=serial_read();
        }
               
        
        for ( i=len-1;i>=0;i--){
            print_int_hex((int)b[i],6);
            serial_write_int_hex((int)b[i],6);
        }
        disp_char('\n');
     
      }
    
          if(function_num==0x02){  
        int i;
         for ( i=len-1;i>=0;i--){
            x_P[i]=serial_read();
        }
               
         to_mongomery (x_P,len,R2,len,p,len,x_P_M,len);
        for ( i=len-1;i>=0;i--){
            print_int_hex((int)x_P[i],6);
            serial_write_int_hex((int)x_P[i],6);
        }
        disp_char('\n');
      }
    
        if(function_num==0x03){  //Crypt with AES 128
        int i;
        for ( i=len-1;i>=0;i--){
            y_P[i]=serial_read();
        }
               
          to_mongomery (y_P,len,R2,len,p,len,y_P_M,len);
        for ( i=len-1;i>=0;i--){
            print_int_hex((int)y_P[i],6);
            serial_write_int_hex((int)y_P[i],6);
        }
        disp_char('\n');
      }
    

         if(function_num==0x06){  
        int i;
       SpeedupScalarMultiply(  a_M,len, b,len, p,len,
     x_P_M,len, y_P_M,len,shifter,len, 
    ALICE_x,len,   ALICE_y,len); 
  from_mongomery(ALICE_x,len,  p,len,ALICE_x,len);
 from_mongomery(ALICE_y,len,  p,len,ALICE_y,len);

        for ( i=len-1;i>=0;i--){
            print_int_hex((int)ALICE_x[i],6);
            serial_write_int_hex((int)ALICE_x[i],6);
        }
         disp_char('\n');
         
          for ( i=len-1;i>=0;i--){
            print_int_hex((int)ALICE_y[i],6);
            serial_write_int_hex((int)ALICE_y[i],6);
        }
         disp_char('\n');
         
         memcpy(ALICE_ka,shifter,len);
         
         
            for ( i=len-1;i>=0;i--){
            print_int_hex((int)ALICE_ka[i],6);
            serial_write_int_hex((int)ALICE_ka[i],6);
        }
         disp_char('\n');
      
      }
    
    
         if(function_num==0x07){  
           int i;
         for ( i=len-1;i>=0;i--){
            BOB_x[i]=serial_read();
        }
        for ( i=len-1;i>=0;i--){
            BOB_y[i]=serial_read();
        }
      
   to_mongomery (BOB_x,len,R2,len,p,len,BOB_x,len);
  to_mongomery (BOB_y,len,R2,len,p,len,BOB_y,len);   
               
      SpeedupScalarMultiply(  a_M,len, b,len, p,len,
   BOB_x,len, BOB_y,len,ALICE_ka,len, 
    res_x,len,   res_y,len);
        
   from_mongomery(res_x,len,  p,len,res_x,len);
 from_mongomery(res_y,len,  p,len,res_y,len);

      
        for ( i=len-1;i>=0;i--){
            print_int_hex((int)res_x[i],6);
            serial_write_int_hex((int)res_x[i],6);
        }
         disp_char('\n');
          for ( i=len-1;i>=0;i--){
            print_int_hex((int)res_y[i],6);
            serial_write_int_hex((int)res_y[i],6);
        }
        disp_char('\n');
      }
    
    
    
    
   shift(shifter,10,mask,10); 
   }

return 0;
}

